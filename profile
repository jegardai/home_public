# Default .profile from gardouille.home_public
# https://git.ipr.univ-rennes.fr/jegardai/home_public

# Manage dependencies {{{
## Ensure to have git {{{
if [ ! $(command -v git) ]; then
	printf '%b\n' "Install git:"
	sudo aptitude --assume-yes install git
fi
## }}}
## Ensure to have mr {{{
	if [ ! $(command -v mr) ]; then
		printf '%b\n' "Install myrepos:"
		sudo aptitude --assume-yes install myrepos
	fi
## }}}
# }}}

# Get the remote home dir {{{
## Extract a minimal git repository if present
[ ! -d ~/.git ] && [ -f ~/.git.init.tar.gz ] && tar xzf ~/.git.init.tar.gz

if [ -d ~/.git ] && [ $(command -v git) ]; then
	### Ensure to give correct permissions
	chown -R "${USER}":$(id -g "${USER}") -- ~/.git

	### Ensure to pull remote home dir content one time
	if [ ! -f "${HOME}"/.gitconfig ]; then
		printf '%b\n' "Please enter Git login to get the remote homedir content"
		git pull
	fi
fi
## }}}

REPO_DIR="repos"
REPO_PATH="${HOME}/${REPO_DIR}"

[ ! -d "${REPO_PATH}" ] && mkdir -p -- "${REPO_PATH}"

# If no .mrconfig file is available {{{
if [ ! -f "${HOME}"/.mrconfig ]; then
	## Download a basic one
	wget --quiet https://git.ipr.univ-rennes.fr/jegardai/home_public/raw/branch/master/mrconfig -O "${HOME}"/.mrconfig
fi
# }}}
# Update repositories {{{

## If a .git.update flag file already exists
if [ -f "${HOME}"/.git.update ]; then

	### Get the date of the last update
	DATE_MTIME_GIT_FLAG=$(stat --format='%Y' "${HOME}"/.git.update)
	DATE_MAX=$(date --date="2 months ago" "+%s")

	### If the last update is too old
	if [ "${DATE_MTIME_GIT_FLAG}" -lt "${DATE_MAX}" ]; then
		### Update all repositories with mr
		mr update

		### Update access time of git.update flag
		touch "${HOME}"/.git.update
	fi
else
	### No .git.update flag file exists

	### Update all repositories with mr
	mr update

	### Create a flag file to know the last update time
	touch "${HOME}"/.git.update
fi
## }}}

# Create symlinks
## tmux {{{
[ -d "${REPO_PATH}"/gardouille.tmux ] && [ ! -L "${HOME}"/.tmux ] && rm -rf "${HOME}"/.tmux && ln -s "${REPO_DIR}"/gardouille.tmux "${HOME}"/.tmux
[ -L "${HOME}"/.tmux ] && [ ! -L "${HOME}"/.tmux.conf ] && rm -f "${HOME}"/.tmux.conf && ln -s .tmux/tmux.conf "${HOME}"/.tmux.conf
## }}}
## vim {{{
[ -d "${REPO_PATH}"/gardouille.vim ] && [ ! -L "${HOME}"/.vim ] && rm -rf "${HOME}"/.vim && ln -s "${REPO_DIR}"/gardouille.vim "${HOME}"/.vim
[ -L "${HOME}"/.vim ] && [ ! -L "${HOME}"/.vimrc ] && rm -f "${HOME}"/.vimrc && ln -s .vim/vimrc "${HOME}"/.vimrc
## }}}
## zsh {{{
[ -d "${REPO_PATH}"/101010.zsh ] && [ ! -L "${HOME}"/.zsh ] && rm -rf "${HOME}".zsh/ && ln -s "${REPO_DIR}"/101010.zsh "${HOME}"/.zsh
[ -L "${HOME}"/.zsh ] && [ ! -L "${HOME}"/.zshrc ] && rm -f .zshrc && ln -s .zsh/zshrc "${HOME}"/.zshrc
[ -L "${HOME}"/.zsh ] && [ ! -L "${HOME}"/.zshenv ] && rm -f .zshenv && ln -s .zsh/zshenv "${HOME}"/.zshenv
[ -L "${HOME}"/.zsh ] && [ ! -L "${HOME}"/.zlogin ] && rm -f .zlogin && ln -s .zsh/zlogin "${HOME}"/.zlogin
## }}}
## fzf {{{
[ -d "${REPO_PATH}"/fzf ] && [ ! -L "${HOME}"/.fzf ] && rm -rf "${HOME}"/.fzf && ln -s "${REPO_DIR}"/fzf "${HOME}"/.fzf
## }}}
## bin {{{
[ -d "${REPO_PATH}"/gardouille.scripts ] && [ ! -L "${HOME}"/bin ] && rm -rf "${HOME}"/bin && ln -s "${REPO_DIR}"/gardouille.scripts "${HOME}"/bin
## }}}

# Remove useless files
rm -rf "${HOME}"/.bash_* "${HOME}"/.bashrc "${HOME}"/.dmrc "${HOME}"/.Xauthority "${HOME}"/.xsession-errors

# Update vim plugins {{{

## If a plug.update flag exists
if [ -f "${HOME}"/.vim/.plug.update ]; then

	DATE_MTIME_PLUG_FLAG=$(stat --format='%Y' "${HOME}"/.vim/.plug.update)
	DATE_MAX_PLUG=$(date --date="2 months ago" "+%s")

	### If the last update is too old
	if [ "${DATE_MTIME_PLUG_FLAG}" -lt "${DATE_MAX_PLUG}" ]; then
		### Update vim's plugins
		vi +'PlugUpdate --sync' +qa

		### Update access time of plug.update flag
		touch "${HOME}"/.vim/.plug.update
	fi
else
## Otherwise, just try to update/install if ~/.vim exists
	if [ -e "${HOME}"/.vim ]; then
		### Download latest version of plug.vim
		curl --no-progress-meter -fLo "${HOME}"/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

		### Update/install vim's plugins
		vi +'PlugUpdate --sync' +qa

		### Create the flag file to know the last update time
		touch "${HOME}"/.vim/.plug.update
	fi
fi
## }}}

# If ZSH is available and TERM var is set
if [ $(command -v zsh) ] && [ "${TERM}" ]; then
	exec zsh
fi
